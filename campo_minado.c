#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "campo_minado.h"

int main()
{  
    struct dif *dif;

    /*inicializacao*/
    initscr();
    cbreak();
    //raw();
    noecho();
    start_color(); 

    /* pega o tamnho da tela*/
    int yMax, xMax;
    getmaxyx(stdscr, yMax, xMax);

    /*cria uma nova janela*/
    WINDOW *menu = newwin(6, xMax-12, yMax-8, 5);   
    box(menu,0,0); // numeros é o estilos da borda
    refresh();
    wrefresh(menu);
    curs_set(0);

    keypad(menu, true);
    char choices[3][15];
    char option_1[] = "JOGAR: FACIL";
    char option_2[] = "JOGAR: MEDIO";
    char option_3[] = "JOGAR: DIFICIL";
    
    strcpy(choices[0], option_1);    
    strcpy(choices[1], option_2);
    strcpy(choices[2], option_3);  
    
    
    int choice;
    int highlight = 0;   

    /*movimentação pelo menu*/
    while(1) {
        for( int i = 0; i < 3; i++ ) {
            if(i == highlight)
               wattron(menu, A_REVERSE);
            mvwprintw(menu, i + 1, 1, choices[i]);
            wattroff(menu, A_REVERSE);
        }
    
        choice = wgetch(menu);

        switch(choice)
        {
            case KEY_UP:
                highlight--;
                if(highlight == -1)
                    highlight = 0;
                break;
            case KEY_DOWN:
            highlight++;
                if(highlight == 3)
                    highlight =2;
                break;
            default:
                break;
        }
        if(choice == 10) // caso escolha enter          
            break;
        
    }
    /*analisa a escolha do usuário*/
    switch(highlight)
    {
        case 0: //facil
            clear();
            //definições padrão
            dif = (struct dif*)malloc(sizeof(struct dif));
            dif->lin = 9;
            dif->col = 9;
            dif->qtd_bombas = 10;
            struct cel jogo_facil[9][9];

            gera_matriz(dif, jogo_facil);
            escolha_usuario(dif, jogo_facil);            
            
            break;
        case 1: // médio 
            clear();            
            //definições padrão
            dif = (struct dif*)malloc(sizeof(struct dif));
            dif->lin = 16;
            dif->col = 16;
            dif->qtd_bombas = 40;
            struct cel jogo_medio[16][16];


            gera_matriz(dif, jogo_medio);
            escolha_usuario(dif, jogo_medio);
            break;
        case 2: //difícil
            clear();            
            //definições padrão
            dif = (struct dif*)malloc(sizeof(struct dif));
            dif->lin = 20;
            dif->col = 20;
            dif->qtd_bombas = 75;
            struct cel jogo_dificil[20][20];
            
            gera_matriz(dif, jogo_dificil);
            escolha_usuario(dif,jogo_dificil);
            break;        
        default:
            break;
    }

    endwin();
    return 1;  
}
