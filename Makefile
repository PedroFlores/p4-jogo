all: campo_minado

clean:
	-rm *.out

purge:
	-rm campo_minado

campo_minado: campo_minado.c secundarias.c campo_minado.h
	gcc campo_minado.c secundarias.c -o campo_minado.out -Wall -lncurses