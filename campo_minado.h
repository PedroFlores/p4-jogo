/*estruturas*/
#include <ncurses.h>
#define BOMBA -1
#define TRUE 1
#define FALSE 0

struct cel {
    int id;
	int reveal;
	int flag;  
};

struct dif {
	int lin;
	int col;
	int qtd_bombas;
};

/*funcoes*/
void gera_matriz(struct dif *dif, struct cel jogo[dif->lin][dif->col]);
void adiciona_bombas(struct dif *dif, struct cel jogo[dif->lin][dif->col]);
void conta_vizinhos(struct dif *dif, struct cel jogo[dif->lin][dif->col]);
int abre_celula(struct dif *dif, int lin, int col, struct cel jogo[dif->lin][dif->col]);
void abre_celulas_adjacentes(struct dif *dif, int lin, int col, struct cel jogo[dif->lin][dif->col]);
int checa_vitoria(struct dif *dif, struct cel jogo[dif->lin][dif->col]);
int checa_derrota(struct dif *dif, struct cel jogo[dif->lin][dif->col]);
void imprime_jogo(struct dif *dif, struct cel jogo[dif->lin][dif->col],WINDOW *win, int continua, int *cel_reveladas);
void imprime_char(struct dif *dif, struct cel celula, WINDOW *win);
void escolha_usuario(struct dif *dif, struct cel jogo[dif->lin][dif->col]);
